#!/bin/bash

set -e

if [ "$#" -ne 5 ]; then
    echo "Need passphrase, proxy password, and FTS3 REST endpoint for integration tests"
    exit 1
fi

passphrase=$1
proxypass=$2
fts3rest=$3
repo_user=$4
repo_passwd=$5


GIT_REPO=${GIT_REPO:="https://gitlab.cern.ch/fts/fts3.git"}
GIT_BRANCH=${GIT_BRANCH:="develop"}


set -x
kinit ${repo_user}@CERN.CH <<EOF
${repo_passwd}
EOF
eosfusebind

function run_mock()
{
    /usr/bin/mock --disable-plugin=tmpfs --configdir=../mock-config/ -r dev-epel-7-x86_64 "$@"
}


if [ -z "$SKIP_INIT" ]; then
    run_mock init
fi
run_mock install java-1.8.0-openjdk rats vera++ cppcheck wget gnupg2 git lcov swig python-virtualenv python-pip m2crypto python-m2ext
run_mock install openssl voms-clients voms-config-vo-dteam lcg-CA
run_mock install gfal2-plugin-mock
if [ ! -d "src" ]; then
    mkdir src
fi
cp /eos/workspace/f/fts/repo/www/repos/testing/el7/x86_64/fts-3*.src.* src/
run_mock installdeps `ls -1 ./src/* | tail -1`

run_mock chroot 'wget "http://repo1.maven.org/maven2/org/codehaus/sonar/runner/sonar-runner-dist/2.4/sonar-runner-dist-2.4.zip"'
run_mock chroot 'unzip -o "sonar-runner-dist-2.4.zip"'

cp /eos/workspace/f/fts/repo/qa/sonar-runner.properties.gpg .
run_mock --copyin "sonar-runner.properties.gpg" /
run_mock chroot "gpg --batch --yes --passphrase ${passphrase} --output ./sonar-runner-2.4/conf/sonar-runner.properties -d sonar-runner.properties.gpg"

if [ ! -d "cert" ]; then
    mkdir cert
fi
cp /eos/workspace/f/fts/repo/cert/ftssuite/*.pem cert/
run_mock --copyin ./cert/*.pem /
run_mock chroot "echo ${proxypass} | voms-proxy-init --voms dteam:/dteam/Role=lcgadmin -pwstdin --cert /robotcert.pem --key /robotkey.pem --out /tmp/proxy.pem"
run_mock chroot "echo ${proxypass} | openssl rsa -in /robotkey.pem -out /robotkey-no.pem -passin stdin"
run_mock chroot "chown root.root /*.pem"
run_mock chroot "chmod 0400 /*.pem"

run_mock chroot 'rm -rfv fts3'
run_mock chroot "git clone \"${GIT_REPO}\" -b ${GIT_BRANCH}"
if [ ! -d "gpg" ]; then
    mkdir gpg
fi
cp /eos/workspace/f/fts/repo/gpg/* gpg/
run_mock --copyin ./gpg/fts*.gpg /
run_mock chroot "gpg --batch --yes --passphrase ${passphrase} --output /fts3/fts3config.conf -d fts3config.gpg"
run_mock chroot "gpg --batch --yes --passphrase ${passphrase} --output /fts3/ftsmsg.conf -d ftsmsg.gpg"
run_mock chroot "cd /fts3; ./qa.sh; X509_USER_CERT=/robotcert.pem X509_USER_KEY=/robotkey-no.pem X509_USER_PROXY=/tmp/proxy.pem FTS3_HOST=${fts3rest} ./coverage.sh; export SONAR_RUNNER_OPTS=\"-Duser.timezone=+01:00 -Djava.security.egd=file:///dev/urandom\"; /sonar-runner-2.4/bin/sonar-runner -X"
rm -rf gpg
rm -rf cert
rm -rf src
