#!/bin/bash
# Just exit on failure
set -e

# Defaults
RPM_LIST=()
REPO_BASE=
REPO_INFIX=
MOCK_CONFIG=

# Read options
while [ -n "$1" ]; do
    case "$1" in
        --repo-base)
            shift
            REPO_BASE="$1"
            ;;
        --repo-infix)
            shift
            REPO_INFIX="$1"
            ;;
        --mock-config)
            shift
            MOCK_CONFIG="$1"
            ;;
        *)
            RPM_LIST+=("$1")
    esac
    shift
done

# Validate
if [ -z "${REPO_BASE}" ]; then
    echo "Missing repository base path"
    exit 1
fi
if [ -z "${MOCK_CONFIG}" ]; then
    echo "Missing mock configuration file"
    exit 1
fi

# Feedback about options
echo "* Repository base:    ${REPO_BASE}"
echo "* Mock configuration: ${MOCK_CONFIG}"
echo "* Number of packages: ${#RPM_LIST[@]}"

# Calculate end repo path
DIST=`grep "config_opts\['dist'\] = " "${MOCK_CONFIG}" | awk '{print $3}' | sed "s/'//g"`
ARCH=`grep "config_opts\['target_arch'\] = " "${MOCK_CONFIG}" | awk '{print $3}' | sed "s/'//g"`

if [ -z "${REPO_INFIX}" ]; then
    REPO="${REPO_BASE}/${DIST}/${ARCH}"
else
    REPO="${REPO_BASE}/${DIST}/${REPO_INFIX}/${ARCH}"
fi

echo "* Final repository: ${REPO}"
#mkdir -p "${REPO}"

# First things first, if rpms with the same name exist, fail inmediately
# A new build must have a new version or release number
for package in ${RPM_LIST[@]}; do
    dest="${REPO}/$(basename ${package})"
    if [ -f "${dest}" ]; then
        echo "${dest} already exists!!"
        echo "New build, new version or release!!"
        echo "Abort"
        exit 1
    fi
done

# Package list removing release number
packages=()
for package in ${RPM_LIST[@]}; do
    rpm_name=`basename ${package} | awk 'match($0, /^(([a-zA-Z]+[a-zA-Z0-9]+-?)+([0-9\.]+))/, m) { print m[1]}'`
    packages+=("${rpm_name}")
done

# Remove old releases
for package in ${packages[@]}; do
    rm -vf ${REPO}/${package}-*.rpm
done

# Copy new packages
for package in ${RPM_LIST[@]}; do
    cp -v "${package}" "${REPO}"
done

# Done
exit 0

